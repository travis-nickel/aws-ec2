# AWS-EC2

This repository serves as an automation tool to bring up aws ec2 instances.

## Preparing the environment

Packages required: 

- ansible
- python
- pip
- pip install boto 
- pip install boto3
  
## Create / Set IAM user with programmatic access

1. Sign in to your AWS account
2. 
![step-2](/images/user-guide-1.png)
3. 
![step-3](/images/user-guide-2.png)
4. 
![step-4](/images/user-guide-3.png)
5. 
![step-5](/images/user-guide-4.png)
6. 
![step-6](/images/user-guide-5.png)
7. Take not of the user's "access key" and "secret key"

## Create boto file

**shell command:** 
vim ~/.boto
**content:**
[Credentials]
aws_access_key_id = YOUR_ACCESS_KEY
aws_secret_access_key = YOUR_SECRET_KEY

## Playbook to launch aws ec2 instances

1. cd playbook
2. Define the vars in spinawsec2.yml
3. ansible-playbook spinawsec2.yml 

## Customizing creation of ec2 instances

Using the ansible playbook **spinawsec2.yml** aws ec2 instance creation can be done, however every time when you want to launch remember to change below variable values

- AMI ID
- Region
- Instance Type
- Security Group Name
- SSH Key Pair Name
- Count of instances to be created

To make play book more flexible and interactive comment out vars section and pass the same variables on playbook execution.

Example of passing variables while running ansible playbook:

`ansible-playbook spinawsec2.yml -e instance_type=m4.large -e security_group=WebServers -e image=ami-0080e4c5bc078760e -e region=asia-pacific-4 -e keypair=NVirginia -e count=1` 